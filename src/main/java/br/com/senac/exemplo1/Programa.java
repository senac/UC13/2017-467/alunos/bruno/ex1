
package br.com.senac.exemplo1;
    
import java.util.ArrayList;
import java.util.List;

public class Programa {

    public static void main(String[] args) {

        Pessoa pessoa1 = new Pessoa("Bruno", 19);
        Pessoa pessoa2 = new Pessoa("Junim", 15);
        Pessoa pessoa3 = new Pessoa("Vitim", 18);

        List<Pessoa> lista = new ArrayList<>();
        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);

        Pessoa pessoaMaisNova = getPessoaMaisNova(lista);
        Pessoa pessoaMaisVelha = getPessoaMaisVelha(lista);

        System.out.println("Pessoa mais nova é: " + pessoaMaisNova);
        System.out.println("Pessoa mais velha é: " + pessoaMaisVelha);
    }

    public static Pessoa getPessoaMaisNova(List<Pessoa> lista) {
        Pessoa pessoaMaisNova = lista.get(0);

        for (Pessoa p : lista) {
            if (p.getIdade() < pessoaMaisNova.getIdade()) {
                pessoaMaisNova = p;
            }
        }

        return pessoaMaisNova;
    }

    public static Pessoa getPessoaMaisVelha(List<Pessoa> lista) {
        Pessoa pessoaMaisVelha = lista.get(0);
        
        for (Pessoa p : lista) {
            if (p.getIdade() > pessoaMaisVelha.getIdade()){
                pessoaMaisVelha = p;
            }
        }
        return pessoaMaisVelha;
    }
}
    

