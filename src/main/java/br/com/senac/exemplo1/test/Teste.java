package br.com.senac.exemplo1.test;

import br.com.senac.exemplo1.Pessoa;
import br.com.senac.exemplo1.Programa;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;


public class Teste {
   
    public Teste() {
        
        
    }
    
    @Test
    public void BrunoNaoDeveSerMaisNovo(){
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Bruno", 31);
        Pessoa pessoa2 = new Pessoa("Junior",19);
        Pessoa pessoa3 = new Pessoa("Vitor", 25);
    
        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        Pessoa pessoaMaisNova = Programa.getPessoaMaisNova(lista);
        assertNotEquals(pessoa1, pessoaMaisNova);
    }
    
     @Test
    public void BrunoDeveSerMaisVelho(){
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Bruno", 31);
        Pessoa pessoa2 = new Pessoa("Junior",19);
        Pessoa pessoa3 = new Pessoa("Vitor", 25);
    
        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        Pessoa pessoaMaisVelha = Programa.getPessoaMaisVelha(lista);
        assertNotEquals(pessoa1, pessoaMaisVelha);
    }
    
    @Test
    public void JuniorDeveSerMaisNovo(){
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Bruno", 31);
        Pessoa pessoa2 = new Pessoa("Junior",19);
        Pessoa pessoa3 = new Pessoa("Vitor", 25);
    
        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        Pessoa pessoaMaisNova = Programa.getPessoaMaisNova(lista);
        assertNotEquals(pessoa1, pessoaMaisNova);
    }
    
    @Test
    public void JuniorDeveSerMaisVelho(){
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Bruno", 31);
        Pessoa pessoa2 = new Pessoa("Junior",19);
        Pessoa pessoa3 = new Pessoa("Vitor", 25);
    
        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        Pessoa pessoaMaisVelha = Programa.getPessoaMaisVelha(lista);
        assertNotEquals(pessoa2, pessoaMaisVelha);
    }
    
    @Test
    public void VitorDeveSerMaisVelho(){
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Bruno", 31);
        Pessoa pessoa2 = new Pessoa("Junior",19);
        Pessoa pessoa3 = new Pessoa("Vitor", 25);
    
        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        Pessoa pessoaMaisNova = Programa.getPessoaMaisNova(lista);
        assertNotEquals(pessoa3, pessoaMaisNova);
    }
    
}

